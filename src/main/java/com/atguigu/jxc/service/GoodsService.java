package com.atguigu.jxc.service;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Goods;

import java.util.Map;

public interface GoodsService {


    ServiceVO getCode();


    Map<String, Object> list(Integer page, Integer rows, String codeOrName, Integer goodsTypeId);


    Map<String, Object> getGoodsList(Integer page, Integer rows, String goodsName, Integer goodsTypeId);

    Integer saveOrUpdateGoods(Integer goodsId, Goods goods);

    Integer deleteById(Integer goodsId);

    Map<String, Object> getNoInventoryQuantity(Integer page, Integer rows, String nameOrCode);

    Map<String, Object> getHasInventoryQuantity(Integer page, Integer rows, String nameOrCode);

    Integer saveStock(Integer goodsId, Integer inventoryQuantity, Double purchasingPrice);

    Integer deleteStock(Integer goodsId);

    Map<String, Object> listAlarm();

}
