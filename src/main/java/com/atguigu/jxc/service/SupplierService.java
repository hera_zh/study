package com.atguigu.jxc.service;

import com.atguigu.jxc.entity.Supplier;

import java.util.List;
import java.util.Map;

/**
 * author: Hera
 * date:2023/9/11
 * description:
 */
public interface SupplierService {
    Map<String, Object> getPageList(Integer page, Integer rows, String supplierName);

    Integer saveOrUpdateSupplier(Integer supplierId, Supplier supplier);

    Integer delete(String ids);
}
