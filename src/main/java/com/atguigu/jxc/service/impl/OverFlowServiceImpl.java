package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.GoodsDao;
import com.atguigu.jxc.dao.GoodsTypeDao;
import com.atguigu.jxc.dao.OverFlowDao;
import com.atguigu.jxc.dao.UserDao;
import com.atguigu.jxc.entity.*;
import com.atguigu.jxc.service.OverFlowService;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * author: Hera
 * date:2023/9/12
 * description:
 */
@Slf4j
@Service
public class OverFlowServiceImpl implements OverFlowService {
    @Autowired
    private OverFlowDao overFlowDao;
    @Autowired
    private GoodsDao goodsDao;
    @Autowired
    private UserDao userDao;
    @Transactional
    @Override
    public Integer save(String overflowNumber, OverflowList overflowList, String overflowListGoodsStr, User currentUser) {
        User userInfo = userDao.getUserById(currentUser.getUserId());
        overflowList.setOverflowNumber(overflowNumber);
        overflowList.setUserId(currentUser.getUserId());
        overflowList.setTrueName(userInfo.getTrueName());
        overFlowDao.saveOverflowList(overflowList);
        log.info(overflowList.toString());
        Gson gson = new Gson();
        List<OverflowListGoods> list=gson.fromJson(overflowListGoodsStr,new TypeToken<List<OverflowListGoods>>(){}.getType());
        List<OverflowListGoods> collect = list.stream().map(i -> {
            i.setOverflowListId(overflowList.getOverflowListId());
            Goods goods = goodsDao.getById(i.getGoodsId());
            i.setGoodsTypeId(goods.getGoodsTypeId());
            return i;
        }).collect(Collectors.toList());
        log.info(collect.toString());
        overFlowDao.saveOverflowListGoods(collect);
        return null;
    }

    @Override
    public Map<String, Object> list(String sTime, String eTime, Integer userId) {
        Map<String, Object> map = new HashMap<>();
        List<OverflowList> list=overFlowDao.list(sTime, eTime);
        List<OverflowList> collect = list.stream().map(i -> {
            User user = userDao.getUserById(i.getUserId());
            i.setTrueName(user.getTrueName());
            return i;
        }).collect(Collectors.toList());

        map.put("rows", collect);
        return map;
    }

    @Override
    public Map<String, Object> goodsList(Integer overflowListId) {
        Map<String, Object> map = new HashMap<>();
        List<OverflowListGoods> list=overFlowDao.goodsList(overflowListId);
        map.put("rows", list);
        return map;
    }
}
