package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.GoodsTypeDao;
import com.atguigu.jxc.domain.ErrorCode;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.GoodsType;
import com.atguigu.jxc.entity.Log;
import com.atguigu.jxc.service.GoodsTypeService;
import com.atguigu.jxc.service.LogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @description
 */
@Service
public class GoodsTypeServiceImpl implements GoodsTypeService {

    @Autowired
    private LogService logService;
    @Autowired
    private GoodsTypeDao goodsTypeDao;

    @Override
    public ArrayList<Object> loadGoodsType() {
        logService.save(new Log(Log.SELECT_ACTION, "查询商品类别信息"));


        return this.getAllGoodsType(-1); // 根节点默认从-1开始
    }

    @Override
    public Integer save(GoodsType goodsType) {
        //保存一个商品类别
        //需要判断当前pid的 goodTypeStatus 是否为1（代表非叶子节点），如果是0（代表叶子节点）需要更新为1，
        // 且将新增的goodsType的goodTypeStatus设置为0。
        GoodsType parent = goodsTypeDao.getById(goodsType.getPId());
        if (parent.getGoodsTypeState() == 0) {
            parent.setGoodsTypeState(1);
            goodsTypeDao.updateGoodsTypeState(parent);
        }
        goodsType.setGoodsTypeState(0);
        return goodsTypeDao.save(goodsType);
    }

    @Override
    public Integer delete(Integer goodsTypeId) {
        //如果删除的是一个叶子节点，那么需要判断父节点是否还有子节点，否则需要更新status为0
        //如果删除的是非叶子节点，那么其子节点也需要一起删除
        GoodsType goodsType = goodsTypeDao.getById(goodsTypeId);
        List<GoodsType> brothers = goodsTypeDao.getAllGoodsTypeByParentId(goodsType.getPId());
        if (0 == goodsType.getGoodsTypeState()) {//代表要删除的是叶子节点，非叶子节点不进if
            if (brothers != null && brothers.size() > 1) {
                return goodsTypeDao.delete(goodsTypeId);
            } else {
                GoodsType goodsType1 = new GoodsType();
                goodsType1.setGoodsTypeId(goodsType.getPId());
                goodsType1.setGoodsTypeState(0);
                goodsTypeDao.updateGoodsTypeState(goodsType1);
                return goodsTypeDao.delete(goodsTypeId);
            }
        }
//        else {
//            //这里代表可以删除父节点
//            List<GoodsType> children = goodsTypeDao.getAllGoodsTypeByParentId(goodsType.getGoodsTypeId());
//            List<Integer> ids = children.stream().map(i -> i.getGoodsTypeId()).collect(Collectors.toList());
//            ids.add(goodsTypeId);
//            if (brothers != null && brothers.size() > 1) {
//                return goodsTypeDao.batchDelete(ids);
//            } else {
//                GoodsType goodsType2 = new GoodsType();
//                goodsType2.setGoodsTypeId(goodsType.getPId());
//                goodsType2.setGoodsTypeState(0);
//                goodsTypeDao.updateGoodsTypeState(goodsType2);
//                return goodsTypeDao.batchDelete(ids);
//            }
//        }
        return 0;
    }

    /**
     * 递归查询所有商品类别
     *
     * @return
     */
    public ArrayList<Object> getAllGoodsType(Integer parentId) {

        ArrayList<Object> array = this.getGoodSTypeByParentId(parentId);

        for (int i = 0; i < array.size(); i++) {

            HashMap obj = (HashMap) array.get(i);

            if (obj.get("state").equals("open")) {// 如果是叶子节点，不再递归

            } else {// 如果是根节点，继续递归查询
                obj.put("children", this.getAllGoodsType(Integer.parseInt(obj.get("id").toString())));
            }

        }

        return array;
    }

    /**
     * 根据父ID获取所有子商品类别
     *
     * @return
     */
    public ArrayList<Object> getGoodSTypeByParentId(Integer parentId) {

        ArrayList<Object> array = new ArrayList<>();

        List<GoodsType> goodsTypeList = goodsTypeDao.getAllGoodsTypeByParentId(parentId);

        System.out.println("goodsTypeList" + goodsTypeList);
        //遍历商品类别
        for (GoodsType goodsType : goodsTypeList) {

            HashMap obj = new HashMap<String, Object>();

            obj.put("id", goodsType.getGoodsTypeId());
            obj.put("text", goodsType.getGoodsTypeName());

            if (goodsType.getGoodsTypeState() == 1) {
                obj.put("state", "closed");

            } else {
                obj.put("state", "open");
            }

            obj.put("iconCls", "goods-type");

            HashMap<String, Object> attributes = new HashMap<>();
            attributes.put("state", goodsType.getGoodsTypeState());
            obj.put("attributes", attributes);

            array.add(obj);

        }

        return array;
    }
}
