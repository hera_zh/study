package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.SupplierDao;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.SupplierService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * author: Hera
 * date:2023/9/11
 * description:
 */
@Slf4j
@Service
public class SupplierServiceImpl implements SupplierService {
    @Autowired
    private SupplierDao supplierDao;

    @Override
    public Map<String, Object> getPageList(Integer page, Integer rows, String supplierName) {
        Map<String, Object> map = new HashMap<>();
        Integer total = supplierDao.getTotal(supplierName);
        page = page == 0 ? 1 : page;
        int offSet = (page - 1) * rows;
        List<Supplier> supplierList = supplierDao.getSupplierList(offSet, rows, supplierName);

        map.put("total", total);
        map.put("rows", supplierList);

        return map;
    }

    @Override
    public Integer saveOrUpdateSupplier(Integer supplierId, Supplier supplier) {
        Integer b = 0;
        if (supplierId!=null) {
            //更新
            b = supplierDao.updateSupplier(supplierId, supplier);
            log.info("执行更新结果：{}",b);
        } else {
            //插入
            b = supplierDao.saveSupplier(supplier);
            log.info("执行插入结果：{}",b);
        }
        return b;
    }

    @Override
    public Integer delete(String ids) {
        Integer i = supplierDao.delete(ids);
        return null;
    }
}
