package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.GoodsDao;
import com.atguigu.jxc.dao.GoodsTypeDao;
import com.atguigu.jxc.domain.ErrorCode;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.*;
import com.atguigu.jxc.service.CustomerReturnListGoodsService;
import com.atguigu.jxc.service.GoodsService;
import com.atguigu.jxc.service.LogService;
import com.atguigu.jxc.service.SaleListGoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @description
 */
@Service
public class GoodsServiceImpl implements GoodsService {

    @Autowired
    private GoodsDao goodsDao;
    @Autowired
    private GoodsTypeDao goodsTypeDao;

    @Override
    public ServiceVO getCode() {

        // 获取当前商品最大编码
        String code = goodsDao.getMaxCode();

        // 在现有编码上加1
        Integer intCode = Integer.parseInt(code) + 1;

        // 将编码重新格式化为4位数字符串形式
        String unitCode = intCode.toString();

        for (int i = 4; i > intCode.toString().length(); i--) {

            unitCode = "0" + unitCode;

        }
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS, unitCode);
    }

    /**
     * 分页查询商品库存
     *
     * @param page
     * @param rows
     * @param codeOrName
     * @param goodsTypeId
     * @return
     */
    @Override
    public Map<String, Object> list(Integer page, Integer rows, String codeOrName, Integer goodsTypeId) {
        Map<String, Object> map = new HashMap<>();
        Integer total = goodsDao.getGoodsCount(codeOrName, goodsTypeId);
        page = page == 0 ? 1 : page;
        int offSet = (page - 1) * rows;
        List<Goods> goodsList = goodsDao.getGoodsList(offSet, rows, codeOrName, goodsTypeId);
        List<Integer> ids = goodsList.stream()
                .map(i -> i.getGoodsId()).collect(Collectors.toList());

        List<SaleListGoods> saleList = goodsDao.getSaleNum(ids);
        List<CustomerReturnListGoods> returnList = goodsDao.getReturnNum(ids);
        List<SaleListGoods> totalSales = saleList.stream().map(i -> {
            for (CustomerReturnListGoods returnListGoods : returnList) {
                if (returnListGoods.getGoodsId().equals(i.getGoodsId())) {
                    i.setGoodsNum(i.getGoodsNum() - returnListGoods.getGoodsNum());
                }
            }
            return i;
        }).collect(Collectors.toList());
        List<Goods> collect = goodsList.stream()
                .map(i -> {
                    for (SaleListGoods totalSale : totalSales) {
                        if (totalSale.getGoodsId().equals(i.getGoodsId())) {
                            i.setSaleTotal(totalSale.getGoodsNum());
                        }
                    }
                    return i;
                }).collect(Collectors.toList());
        map.put("total", total);
        map.put("rows", collect);
        return map;
    }

    @Override
    public Map<String, Object> getGoodsList(Integer page, Integer rows, String goodsName, Integer goodsTypeId) {
        Map<String, Object> map = new HashMap<>();
        Integer total = goodsDao.totalGoods(goodsName, goodsTypeId);
        page = page == 0 ? 1 : page;
        int offSet = (page - 1) * rows;
        List<Goods> goodsList = goodsDao.getGoods(offSet, rows, goodsName, goodsTypeId);
        map.put("total", total);
        map.put("rows", goodsList);
        return map;
    }

    @Override
    public Integer saveOrUpdateGoods(Integer goodsId, Goods goods) {
        if (goodsId != null) {
            //更新
            return goodsDao.update(goodsId, goods);
        } else {
            goods.setInventoryQuantity(0);//数据库不能为空，给个初始值
            goods.setState(0);//数据库规定初始值为0，不能为null
            //新增
            return goodsDao.save(goods);
        }
    }

    @Override
    public Integer deleteById(Integer goodsId) {
        Goods goods = goodsDao.getById(goodsId);
        if (0 == goods.getState()) {
            return goodsDao.deleteById(goodsId);
        } else {

            return 0;
        }

    }

    @Override
    public Map<String, Object> getNoInventoryQuantity(Integer page, Integer rows, String nameOrCode) {
        Map<String, Object> map = new HashMap<>();
        page = page == 0 ? 1 : page;
        int offSet = (page - 1) * rows;
        List<Goods> goods = goodsDao.getNoInventoryQuantity(offSet, rows, nameOrCode);
        map.put("total", goods.size());
        map.put("rows", goods);
        return map;
    }

    @Override
    public Map<String, Object> getHasInventoryQuantity(Integer page, Integer rows, String nameOrCode) {
        Map<String, Object> map = new HashMap<>();
        page = page == 0 ? 1 : page;
        int offSet = (page - 1) * rows;
        List<Goods> goods = goodsDao.getHasInventoryQuantity(offSet, rows, nameOrCode);
        map.put("total", goods.size());
        map.put("rows", goods);
        return map;
    }

    @Override
    public Integer saveStock(Integer goodsId, Integer inventoryQuantity, Double purchasingPrice) {
        //不管是修改还是新增库存，前端发过来的数据结构是一样的，需要查询数据库，根据现有库存是否小于等于0来判断是新增或更新
//        Goods target = goodsDao.getById(goodsId);
//        if (target.getInventoryQuantity() <= 0) {
//            //新增
//            target.setInventoryQuantity(inventoryQuantity);
//            target.setPurchasingPrice(purchasingPrice);
//            return goodsDao.save(target);
//        } else {
        //更新
//            target.setInventoryQuantity(inventoryQuantity);
//            target.setPurchasingPrice(purchasingPrice);
        return goodsDao.updateStock(goodsId, inventoryQuantity, purchasingPrice);
//        }
    }

    @Override
    public Integer deleteStock(Integer goodsId) {
        Goods goods = goodsDao.getById(goodsId);
        if (0 == goods.getState()) {
            return goodsDao.deleteById(goodsId);
        } else {
            return 0;
        }
    }

    @Override
    public Map<String, Object> listAlarm() {
        Map<String, Object> map = new HashMap<>();
        List<Goods> goods = goodsDao.listAlarm();
        List<Goods> collect = goods.stream().map(i -> {
            GoodsType type = goodsTypeDao.getById(i.getGoodsTypeId());
            i.setGoodsTypeName(type.getGoodsTypeName());
            return i;
        }).collect(Collectors.toList());
        map.put("rows", collect);
        return map;
    }


}
