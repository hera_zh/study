package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.CustomerDao;
import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * author: Hera
 * date:2023/9/12
 * description:
 */
@Service
public class CustomerServiceImpl implements CustomerService {
    @Autowired
    private CustomerDao customerDao;
    @Override
    public Map<String, Object> list(Integer page, Integer rows, String customerName) {
        Map<String, Object> map = new HashMap<>();
        page=page==0?1:page;
        int offSet=(page-1)*rows;
        List<Customer> customerList=customerDao.list(offSet, rows, customerName);
        map.put("total",customerList.size());
        map.put("rows", customerList);
        return map;
    }

    @Override
    public Integer saveOrUpdate(Integer customerId, Customer customer) {
        Integer i=0;
        if (customerId == null) {
            i = customerDao.save(customer);
        } else {
            i = customerDao.update(customerId, customer);
        }
        return i;
    }

    @Override
    public Integer delete(String ids) {
        return customerDao.delete(ids);
    }
}
