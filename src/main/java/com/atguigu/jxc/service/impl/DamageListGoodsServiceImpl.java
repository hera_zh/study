package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.DamageListGoodsDao;
import com.atguigu.jxc.dao.GoodsDao;
import com.atguigu.jxc.dao.UserDao;
import com.atguigu.jxc.entity.*;
import com.atguigu.jxc.service.DamageListGoodsService;
import com.atguigu.jxc.util.DateUtil;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * author: Hera
 * date:2023/9/12
 * description:
 */
@Service
@Slf4j
public class DamageListGoodsServiceImpl implements DamageListGoodsService {
    @Autowired
    private DamageListGoodsDao damageDao;

    @Autowired
    private UserDao userDao;
    @Autowired
    GoodsDao goodsDao;

    @Transactional
    @Override
    public Integer save(String damageNumber, DamageList damageList, String damageListGoodsStr, User currentUser) {
        User userInfo = userDao.getUserById(currentUser.getUserId());
        damageList.setDamageNumber(damageNumber);
        damageList.setUserId(currentUser.getUserId());
        damageList.setTrueName(userInfo.getTrueName());
        damageDao.saveDamageList(damageList);
        log.info(damageList.toString());
        Gson gson = new Gson();
        List<DamageListGoods> list=gson.fromJson(damageListGoodsStr,new TypeToken<List<DamageListGoods>>(){}.getType());
        List<DamageListGoods> collect = list.stream().map(i -> {
            i.setDamageListId(damageList.getDamageListId());
            Goods goods = goodsDao.getById(i.getGoodsId());
            i.setGoodsTypeId(goods.getGoodsTypeId());
            return i;
        }).collect(Collectors.toList());
        log.info(collect.toString());
        damageDao.saveDamageListGoods(collect);
        return null;
    }

    @Override
    public Map<String, Object> list(String sTime, String eTime, Integer userId)  {
        Map<String, Object> map = new HashMap<>();
//        Date date2 = DateUtil.StringToDate(eTime, "yyyy-MM-dd");
//        Date date1 = DateUtil.StringToDate(sTime, "yyyy-MM-dd");
        List<DamageList> list=damageDao.list(sTime, eTime);
        List<DamageList> collect = list.stream().map(i -> {
            User user = userDao.getUserById(i.getUserId());
            i.setTrueName(user.getTrueName());
            return i;
        }).collect(Collectors.toList());

        map.put("rows", collect);
        return map;
    }

    @Override
    public Map<String, Object> goodsList(Integer damageListId) {
        Map<String, Object> map = new HashMap<>();
        List<DamageListGoods> list=damageDao.goodsList(damageListId);
        map.put("rows", list);
        return map;
    }
}
