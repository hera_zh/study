package com.atguigu.jxc.service;

import com.atguigu.jxc.entity.Customer;

import java.util.Map;

/**
 * author: Hera
 * date:2023/9/12
 * description:
 */
public interface CustomerService {
    Map<String, Object> list(Integer page, Integer rows, String customerName);

    Integer saveOrUpdate(Integer customerId, Customer customer);

    Integer delete(String ids);
}
