package com.atguigu.jxc.service;

import com.atguigu.jxc.entity.OverflowList;
import com.atguigu.jxc.entity.User;

import java.util.Map;

/**
 * author: Hera
 * date:2023/9/12
 * description:
 */
public interface OverFlowService {
    Integer save(String overflowNumber, OverflowList overflowList, String overflowListGoodsStr, User currentUser);

    Map<String, Object> list(String sTime, String eTime, Integer userId);

    Map<String, Object> goodsList(Integer overflowListId);
}
