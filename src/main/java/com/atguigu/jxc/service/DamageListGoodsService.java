package com.atguigu.jxc.service;

import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.User;

import java.text.ParseException;
import java.util.Map;

/**
 * author: Hera
 * date:2023/9/12
 * description:
 */
public interface DamageListGoodsService {
    Integer save(String damageNumber, DamageList damageList, String damageListGoodsStr, User currentUser);

    Map<String, Object> list(String sTime, String eTime, Integer userId);

    Map<String, Object> goodsList(Integer damageListId);
}
