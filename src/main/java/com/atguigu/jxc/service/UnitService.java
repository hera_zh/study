package com.atguigu.jxc.service;

import java.util.Map;

/**
 * author: Hera
 * date:2023/9/11
 * description:
 */
public interface UnitService {
    Map<String, Object> getUnitList();

}
