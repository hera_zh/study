package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.SupplierService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.java.Log;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * author: Hera
 * date:2023/9/11
 * description:
 */
@Api(tags = "供应商管理")
@RestController
@RequestMapping("/supplier")
@Slf4j
public class SupplierController {

    @Autowired
    private SupplierService supplierService;

    @ApiOperation(value = "分页查询供应商")
    @PostMapping("/list")
    public Map<String, Object> getPageList(@RequestParam("page") Integer page,
                                           @RequestParam("rows") Integer rows,
                                           @RequestParam(value = "supplierName", required = false) String supplierName) {
        return supplierService.getPageList(page, rows, supplierName);
    }

    @ResponseBody
    @PostMapping("/save")
    public ServiceVO saveOrUpdateSupplier(@RequestParam(value = "supplierId", required = false) Integer supplierId,
                                          Supplier supplier) {
        Integer b = supplierService.saveOrUpdateSupplier(supplierId, supplier);
        log.info("执行结果：{}", b);
        return new ServiceVO(100, "请求成功", null);
    }

    @PostMapping("/delete")
    public ServiceVO delete(String ids) {
       Integer i= supplierService.delete(ids);

        return new ServiceVO(100, "请求成功", null);
    }
}
