package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * author: Hera
 * date:2023/9/12
 * description:
 */
@RestController
@RequestMapping("/customer")
public class CustomerController {
    @Autowired
    private CustomerService customerService;

    @PostMapping("/list")
    public Map<String, Object> list(@RequestParam("page") Integer page,
                                    @RequestParam("rows") Integer rows,
                                    @RequestParam(value = "customerName", required = false) String customerName) {

        return customerService.list(page, rows, customerName);
    }

    @PostMapping("/save")
    public ServiceVO saveOrUpdate(@RequestParam(value = "customerId", required = false) Integer customerId,
                                  Customer customer) {
        Integer i = customerService.saveOrUpdate(customerId, customer);

        return new ServiceVO(100, "请求成功", null);
    }

    @PostMapping("/delete")
    public ServiceVO delete(String ids) {
        Integer i=customerService.delete(ids);
        return new ServiceVO(100, "请求成功", null);
    }
}
