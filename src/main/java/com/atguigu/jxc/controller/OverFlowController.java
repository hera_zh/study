package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.OverflowList;
import com.atguigu.jxc.entity.User;
import com.atguigu.jxc.service.OverFlowService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.text.ParseException;
import java.util.Map;

/**
 * author: Hera
 * date:2023/9/12
 * description:
 */
@RestController
@RequestMapping("/overflowListGoods")
public class OverFlowController {

    @Autowired
    private OverFlowService overFlowService;
    @PostMapping("/save")
    public ServiceVO save(@RequestParam("overflowNumber") String overflowNumber,
                          OverflowList overflowList, String overflowListGoodsStr, HttpSession session) {
        User currentUser = (User) session.getAttribute("currentUser");
        Integer i=overFlowService.save(overflowNumber,overflowList,overflowListGoodsStr,currentUser);
        return new ServiceVO(100, "请求成功", null);
    }
    @PostMapping("/list")
    public Map<String, Object> list(String sTime, String eTime, HttpSession session){
        User currentUser = (User) session.getAttribute("currentUser");
        Integer userId = currentUser.getUserId();
        return overFlowService.list(sTime, eTime, userId);
    }

    @PostMapping("/goodsList")
    public Map<String, Object> goodsList(@RequestParam("overflowListId") Integer overflowListId) {
        return overFlowService.goodsList(overflowListId);
    }
}
