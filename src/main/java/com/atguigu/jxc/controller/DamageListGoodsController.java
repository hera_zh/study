package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.OverflowList;
import com.atguigu.jxc.entity.User;
import com.atguigu.jxc.service.DamageListGoodsService;
import com.atguigu.jxc.service.OverFlowService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.text.ParseException;
import java.util.Map;

/**
 * author: Hera
 * date:2023/9/12
 * description:
 */
@RestController
@RequestMapping("/damageListGoods")
public class DamageListGoodsController {
    ///save?damageNumber=BS1605766644460
    @Autowired
    private DamageListGoodsService damageListGoodsService;

    @PostMapping("/save")
    public ServiceVO save(@RequestParam("damageNumber") String damageNumber,
                          DamageList damageList, String damageListGoodsStr, HttpSession session) {
        User currentUser = (User) session.getAttribute("currentUser");
        Integer i = damageListGoodsService.save(damageNumber, damageList, damageListGoodsStr, currentUser);
        return new ServiceVO(100, "请求成功", null);
    }

    @PostMapping("/list")
    public Map<String, Object> list(String sTime, String eTime, HttpSession session) {
        User currentUser = (User) session.getAttribute("currentUser");
        Integer userId = currentUser.getUserId();
        return damageListGoodsService.list(sTime, eTime, userId);
    }

    @PostMapping("/goodsList")
    public Map<String, Object> goodsList(@RequestParam("damageListId") Integer damageListId) {
        return damageListGoodsService.goodsList(damageListId);
    }
}
