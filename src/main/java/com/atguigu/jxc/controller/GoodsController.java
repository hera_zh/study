package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.service.GoodsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * @description 商品信息Controller
 */
@Api(tags = "商品信息controller")
@RestController
@RequestMapping("/goods")
public class GoodsController {

    @Autowired
    private GoodsService goodsService;

    /**
     * 分页查询商品库存信息  列出库存
     *
     * @param page        当前页
     * @param rows        每页显示条数
     * @param codeOrName  商品编码或名称
     * @param goodsTypeId 商品类别ID
     * @return  json
     */
    @ApiOperation(value = "首页商品库存分页查询")
//    @ResponseBody
    @PostMapping("/listInventory")
    public Map<String, Object> listInventory(@RequestParam("page") Integer page,
                                             @RequestParam("rows") Integer rows,
                                             @RequestParam(value = "codeOrName",required = false) String codeOrName,
                                             @RequestParam(value = "goodsTypeId",required = false) Integer goodsTypeId) {
        return goodsService.list(page,rows,codeOrName,goodsTypeId);
    }

    /**
     * 分页查询商品信息
     * @param page 当前页
     * @param rows 每页显示条数
     * @param goodsName 商品名称
     * @param goodsTypeId 商品类别ID
     * @return
     */
    @ResponseBody
    @PostMapping("/list")
    public Map<String,Object> getGoodsList(@RequestParam("page") Integer page,
                                           @RequestParam("rows") Integer rows,
                                           @RequestParam(value = "goodsName",required = false) String goodsName,
                                           @RequestParam(value = "goodsTypeId",required = false) Integer goodsTypeId) {
        return goodsService.getGoodsList(page,rows,goodsName,goodsTypeId);
    }


    /**
     * 生成商品编码
     * @return
     */
    @RequestMapping("/getCode")
    @RequiresPermissions(value = "商品管理")
    public ServiceVO getCode() {
        return goodsService.getCode();
    }

    /**
     * 添加或修改商品信息
     *
     * @param goods 商品信息实体
     * @return
     */
    @PostMapping("/save")
    public ServiceVO saveOrUpdateGoods(@RequestParam(value = "goodsId",required = false) Integer goodsId,
                                       Goods goods) {
        Integer i=goodsService.saveOrUpdateGoods(goodsId, goods);
        return new ServiceVO(100, "请求成功", null);
    }

    /**
     * 删除商品信息
     *
     * @param goodsId 商品ID
     * @return
     */
    @PostMapping("/delete")
    public ServiceVO deleteById(Integer goodsId) {
        Integer i=goodsService.deleteById(goodsId);
        if (i == 0) {
            return new ServiceVO(101, "请求失败,原因：商品已入库、有进货或者有销售单据", null);
        }
        return new ServiceVO(100, "请求成功", null);
    }

    /**
     * 分页查询无库存商品信息
     *
     * @param page       当前页
     * @param rows       每页显示条数
     * @param nameOrCode 商品名称或商品编码
     * @return
     */
    @PostMapping("/getNoInventoryQuantity")
    public Map<String, Object> getNoInventoryQuantity(@RequestParam("page") Integer page,
                                                      @RequestParam("rows") Integer rows,
                                                      @RequestParam(value = "nameOrCode", required = false) String nameOrCode) {
        return goodsService.getNoInventoryQuantity(page, rows, nameOrCode);
    }


    /**
     * 分页查询有库存商品信息
     * @param page 当前页
     * @param rows 每页显示条数
     * @param nameOrCode 商品名称或商品编码
     * @return
     */
    @PostMapping("/getHasInventoryQuantity")
    public Map<String, Object> getHasInventoryQuantity(@RequestParam("page") Integer page,
                                                      @RequestParam("rows") Integer rows,
                                                      @RequestParam(value = "nameOrCode", required = false) String nameOrCode) {
        return goodsService.getHasInventoryQuantity(page, rows, nameOrCode);
    }

    /**
     * 添加商品期初库存
     *
     * @param goodsId           商品ID
     * @param inventoryQuantity 库存
     * @param purchasingPrice   成本价
     * @return
     */
    @PostMapping("/saveStock?goodsId=25")
    public ServiceVO saveStock(@RequestParam("goodsId")Integer goodsId,
                               Integer inventoryQuantity,
                               Double purchasingPrice) {

        Integer i=goodsService.saveStock(goodsId,inventoryQuantity,purchasingPrice);
        return new ServiceVO(100, "请求成功", null);
    }

    /**
     * 删除商品库存
     * @param goodsId 商品ID
     * @return
     */@PostMapping("/deleteStock")
    public ServiceVO deleteStock(Integer goodsId) {
        Integer i = goodsService.deleteStock(goodsId);
        if (i == 0) {
            return new ServiceVO(101, "请求失败,原因：商品已入库、有进货或者有销售单据", null);
        }
        return new ServiceVO(100, "请求成功", null);
    }

    /**
     * 查询库存报警商品信息
     *
     * @return
     */
    @PostMapping("/listAlarm")
    public Map<String, Object> listAlarm() {
        return goodsService.listAlarm();
    }

}
