package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.GoodsType;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @description 商品类别
 */
@Repository
public interface GoodsTypeDao {

    List<GoodsType> getAllGoodsTypeByParentId(Integer pId);

    Integer updateGoodsTypeState(GoodsType parentGoodsType);


    Integer save(@Param("goodsType") GoodsType goodsType);

//    GoodsType getByPid(@Param("pId") Integer pId);

    Integer delete(@Param("goodsTypeId") Integer goodsTypeId);

    GoodsType getById(@Param("goodsTypeId") Integer goodsTypeId);

    Integer batchDelete(@Param("ids") List<Integer> ids);
}
