package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.CustomerReturnListGoods;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.SaleListGoods;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * @description 商品信息
 */
@Repository
public interface GoodsDao {


    String getMaxCode();


    Integer getGoodsCount(@Param("codeOrName") String codeOrName,@Param("goodsTypeId") Integer goodsTypeId);

    List<Goods> getGoodsList(@Param("offSet") int offSet,
                             @Param("rows") Integer rows,
                             @Param("codeOrName") String codeOrName,
                             @Param("goodsTypeId") Integer goodsTypeId);

    List<SaleListGoods> getSaleNum(@Param("ids") List<Integer> ids);

    List<CustomerReturnListGoods> getReturnNum(@Param("ids") List<Integer> ids);

    Integer totalGoods(@Param("goodsName") String goodsName, @Param("goodsTypeId") Integer goodsTypeId);

    List<Goods> getGoods(@Param("offSet") int offSet, @Param("rows") Integer rows, @Param("goodsName") String goodsName, @Param("goodsTypeId") Integer goodsTypeId);

    Integer update(@Param("goodsId") Integer goodsId, @Param("goods") Goods goods);

    Integer save(@Param("goods") Goods goods);

    Goods getById(@Param("goodsId") Integer goodsId);

    Integer deleteById(@Param("goodsId") Integer goodsId);

    List<Goods> getNoInventoryQuantity(@Param("offSet") int offSet, @Param("rows") Integer rows, @Param("nameOrCode") String nameOrCode);

    List<Goods> getHasInventoryQuantity(@Param("offSet") int offSet, @Param("rows") Integer rows, @Param("nameOrCode") String nameOrCode);

    Integer updateStock(@Param("goodsId") Integer goodsId, @Param("inventoryQuantity") Integer inventoryQuantity, @Param("purchasingPrice") Double purchasingPrice);


    List<Goods> listAlarm();
}
