package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Customer;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * author: Hera
 * date:2023/9/12
 * description:
 */
@Repository
public interface CustomerDao {

    List<Customer> list(@Param("offSet") int offSet, @Param("rows") Integer rows, @Param("customerName") String customerName);

    Integer save(@Param("customer") Customer customer);

    Integer update(@Param("customerId") Integer customerId, @Param("customer") Customer customer);

    Integer delete(@Param("ids") String ids);
}
