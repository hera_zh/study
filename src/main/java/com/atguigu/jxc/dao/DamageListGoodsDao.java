package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.DamageListGoods;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * author: Hera
 * date:2023/9/12
 * description:
 */
@Repository
public interface DamageListGoodsDao {
    Integer saveDamageList(DamageList damageList);

    void saveDamageListGoods(@Param("collect") List<DamageListGoods> collect);

    List<DamageList> list(@Param("sTime") String sTime, @Param("eTime") String eTime);

    List<DamageListGoods> goodsList(@Param("damageListId") Integer damageListId);
}
