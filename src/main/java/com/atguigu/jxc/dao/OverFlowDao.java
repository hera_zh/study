package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.OverflowList;
import com.atguigu.jxc.entity.OverflowListGoods;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * author: Hera
 * date:2023/9/12
 * description:
 */
@Repository
public interface OverFlowDao {
    Integer saveOverflowList(OverflowList overflowList);

    Integer saveOverflowListGoods(@Param("list") List<OverflowListGoods> list);

    List<OverflowList> list(@Param("sTime") String sTime, @Param("eTime") String eTime);

    List<OverflowListGoods> goodsList(@Param("overflowListId") Integer overflowListId);
}
