package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Supplier;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * author: Hera
 * date:2023/9/11
 * description:
 */
@Repository
public interface SupplierDao {
    Integer getTotal(@Param("supplierName") String supplierName);

    List<Supplier> getSupplierList(@Param("offSet") int offSet, @Param("rows") Integer rows, @Param("supplierName") String supplierName);

    Integer updateSupplier(@Param("supplierId") Integer supplierId, @Param("supplier") Supplier supplier);

    Integer saveSupplier(@Param("supplier") Supplier supplier);

    Integer delete(@Param("ids") String ids);
}
