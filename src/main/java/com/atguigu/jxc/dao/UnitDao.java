package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Unit;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * author: Hera
 * date:2023/9/11
 * description:
 */
@Repository
public interface UnitDao {
    List<Unit> getUnitList();
}
